#' ---
#' title: We R Live 07: Introdução à Estatística Espacial I
#' author: felipe sodre
#' date: 2020-06-16
#' ---

# prepare r -------------------------------------------------------------
# packages
# Carregando pacotes -
library(rgdal)

#install.packages('spatstat')
library(spatstat)
library(maptools)
library(raster)
library(sf)

library(readr)
library(dplyr)

library(geobr)

library(tmap)

# Load data ----
fogocruzado <- read_csv("./data/fogocruzado.csv") %>% 
  st_as_sf(coords = c("longitude_ocorrencia", "latitude_ocorrencia"), crs = 4326) %>% 
  st_transform(crs = 32723)
glimpse(fogocruzado)

# Limite Municipio RJ - GeoBr
rj <- read_municipality(code_muni = 3304557) %>% 
  st_transform(crs = 32723)

# plot
tmap_mode("view")
tm_shape(fogocruzado) +
  tm_dots() + 
  tm_shape(rj)+
  tm_borders() + 
  tm_basemap(leaflet::providers$OpenStreetMap)


# First Order Analysis (Analise de primeira ordem) ----

# de sf a classe sp
class(rj)
rj_sp <- as(rj, "Spatial") # convertendo sf para classe sp
class(rj_sp)

# convertendo a objeto da classe "owin"
rj_win <- as.owin(rj_sp)

# de sf a classe sp
fogocruzado_sp <- as(fogocruzado, "Spatial")

# Construindo objeto classe ppp
head(fogocruzado_sp@coords)
fc_ppp <- spatstat::ppp(
  fogocruzado_sp@coords[,1], 
  fogocruzado_sp@coords[,2], 
  window = rj_win)

class(fc_ppp)

# Sumario estatistico ----
summary(fc_ppp)

# Plot
plot(fc_ppp)

# Kernel ---

# Raio de busca arbitrário
Kernel <- density.ppp(fc_ppp, sigma = 1000)
plot(Kernel)

# Considerando abordagens estatística
# bw = bandwidth
raio_diggle <- bw.diggle(fc_ppp)
raio_scott <- bw.scott(fc_ppp)

Kernel_diggle <- density.ppp(fc_ppp, sigma = raio_diggle)
Kernel_scott <- density.ppp(fc_ppp, sigma = mean(raio_scott))

class(Kernel_scott) # classse imagem e não raster

# Comparando resultados
plot(Kernel_diggle, main = "raio baseado em Diggle 1989")
plot(Kernel_scott, main = "raio baseado em Scott 1992")

# Convertendo kernel para raster ---

Kernel_scott <- raster(Kernel_scott)
class(Kernel_scott)

crs(Kernel_scott) <- crs(fogocruzado)

tmap_mode("plot")
tm_shape(Kernel_scott) +
  # style = "fisher"
  tm_raster(title = '') +
  #tm_shape(fogocruzado) +
  #tm_dots() + 
  tm_shape(rj) +
  tm_borders() +
  tm_legend(legend.outside = T) +
  tm_graticules(lwd = 0) + 
  tm_layout(main.title = "Distribuição tiroteios 2019")
#tmap_save(filename = "./img/kernel.png")

# end ---