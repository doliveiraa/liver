---
date: "2020-06-23T22:00-23:00"
tags:
- r
- rspatial
- processo pontual
- spatstat
title: We R Live 08 - Introdução à Estatística Espacial II
---

{{<youtube BCl_V-SpQec>}}

Dando seguimento à série de lives sobre estatística espacial, vamos apresentar um pouco sobre análises de dependência espacial para processos pontuais.

Slides: https://werlive.netlify.app/werlive08/werlive08

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive08/werlive08.R

Vídeo: https://youtu.be/BCl_V-SpQec