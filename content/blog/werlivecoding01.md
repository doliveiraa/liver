---
date: "2020-08-12T22:00-00:00"
tags:
- r
title: We R Live Coding 01 - Insomnia
---

{{<youtube I4ErDOwdXrk>}}


Experimentando um pouco este formato: Live coding

Reproduzir uma análise feita pelo Narcélio: https://twitter.com/NarceliodeSa/status/1289213851750170625

Repositório: https://gitlab.com/geocastbrasil/we-r-live-coding

Vídeo: https://youtu.be/I4ErDOwdXrk