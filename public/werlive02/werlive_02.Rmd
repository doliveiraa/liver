---
title: "We R Live 02: Elaborando mapas no R <br><br>"
subtitle: "<br>GeoCastBrasil" 
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 05/05/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
library(geobr)
library(sf)
library(ggplot2)
library(ggspatial)
```

background-image: url(img/logo_werlive.png)
background-size: 300px
background-position: 90% 90%

# We R Live 02

## Tópicos
### <u><b>Introdução: 15 min.</b></u>
### 1 Desafio da LiveR 2
### 2 Pacotes a serem usados
### 3 Considerações sobre a sintaxe

--

### <u><b>Mão na massa: 30/40 min.</b></u>
### 4 Carregar dados vetoriais
### 5 Mapas usando ggplot2
### 6 Considerações finais (5 min)

---

class: inverse, middle, center

# 1 Desafio da LiveR 2

---

# 1 Desafio da LiveR 2

### Elaborar um <b>mapa de uso e cobertura da terra</b> com os princiais elementos cartográticos
```{r,,echo=FALSE,message=FALSE,results='hide',fig.align='center',out.width='60%'}
# land use and choose colors + rio claro limit fill + coords + themes + scalebar + north
polygons_land_use <- sf::read_sf("data/SP_3543907_USO.shp")
rio_claro_limit <- geobr::read_municipality(code_muni = 3543907, year = 2015)
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green")) +
  coord_sf(datum = sf::st_crs(polygons_land_use)) +
  theme_bw() +
  annotation_scale(location = "br", width_hint = .3) +
  annotation_north_arrow(location = "br", which_north = "true", 
                         pad_x = unit(0, "cm"), pad_y = unit(.8, "cm"),
                         style = north_arrow_fancy_orienteering)
```

---

# Script para essa live

<br><br><br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/werlive_02/werlive_02.R`]

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(img/package_geobr.png),url(img/package_sf.gif),url(img/package_ggplot2.png)
background-size: 200px,230px,200px
background-position: 10% 85%,50% 85%,90% 85%

# 2 Pacotes a serem usados

## Instalar e carregar pacotes
```{r,,eval=FALSE}
# packages
library(geobr) # ibge limits
library(sf) # vector
library(ggplot2) # graphics and maps
library(ggspatial) # spatial elements in ggplot2
```

---

class: inverse, middle, center

# 3 Considerações sobre a sintaxe

---

# 3 Considerações sobre a sintaxe

## Comentários (*#*)

Comentários **não são lidos** pelo R e **descrevem informações**

--

### Cabeçalho

```{r}
#' ---
#' title: vetores e mapas no r
#' author: mauricio vancine
#' date: 2020-05-05
#' ---
```

--

### Informações sobre os comandos

```{r}
## comentarios
# o r nao le o codigo depois do # (hash)

42 # essas palavras nao sao executadas, apenas o 42
```

---

background-image: url(img/general_comment_gandalf.jpg)
background-size: 350px
background-position: 80% 80%

# 3 Considerações sobre a sintaxe

## Por que comentar meu código?

- Didática

- Reprodutibilidade

- Legibilidade

- Memorização

- Código ideal: 

50% Códigos | 50% Comentários

---

# 3 Considerações sobre a sintaxe

## Diretório de trabalho

Endereço da pasta onde o R irá **importar e exportar** os dados

**Atalho**: `ctrl + shift + H`

### **Windows: inverter as barras ("\" por "/")!**

```{r,,eval=FALSE}
## diretorio de trabalho
# pasta onde o r ira importar e exportar os arquivos

# definir o diretorio de trabalho
setwd("/home/mude/data/gitlab/liver/werlive_02/data")
```
--
```{r,,eval=FALSE}
# verificar o diretorio
getwd()
```

---

class: clear, inverse, center, middle

# Vamos trabalhar com dados reais?

---

class: clear, inverse, center, middle

background-image: url(img/general_real_data.jpg)
background-size: 600px
background-position: 50% 50%

---

class: inverse, middle, center

# 4 Carregar dados vetoriais

---

background-image: url(img/data_fbds.png)
background-size: 500px
background-position: 50% 90%

# 4 Carregar dados vetoriais

### Fundação Brasileira Desenvolvimento Sustentável (FBDS)

Em 2015, a FBDS deu início ao *Projeto de Mapeamento em Alta Resoluçăo dos Biomas Brasileiros*, que vem produzindo dados primários de uso e cobertura do solo, hidrografia e Áreas de Preservaçăo Permanente (APP) em resoluçăo de 5 metros. O mapeamento foi concluído para mais de 4 mil municípios dos biomas *Mata Atlântica e Cerrado*

site: https://www.fbds.org.br/<br>
repositório: http://geo.fbds.org.br/

---

# 4 Carregar dados vetoriais

## Download de dados pelo R
```{r,,eval=FALSE}
# polygons - land use
download.file(url = "http://geo.fbds.org.br/SP/RIO_CLARO/USO/SP_3543907_USO.dbf", 
              destfile = "SP_3543907_USO.dbf")
download.file(url = "http://geo.fbds.org.br/SP/RIO_CLARO/USO/SP_3543907_USO.prj", 
              destfile = "SP_3543907_USO.prj")
download.file(url = "http://geo.fbds.org.br/SP/RIO_CLARO/USO/SP_3543907_USO.shp", 
              destfile = "SP_3543907_USO.shp")
download.file(url = "http://geo.fbds.org.br/SP/RIO_CLARO/USO/SP_3543907_USO.shx", 
              destfile = "SP_3543907_USO.shx")
```

---

class: clear, center, middle
background-image: url(img/package_sf.gif)
background-size: 400px
background-position: 50% 50%

---

background-image: url(img/stats_illustrations_sf_pt.png)
background-size: 400px
background-position: 90% 90%

# 4 Carregar dados vetoriais

## **sf**

--

### Desenvolvido à partir do pacote `sp`

--

### Integrado ao **tidyverse** (pipes, tidyr, dplyr, ...)

--

### Possibilita **operações com atributos, espaciais e geométricas** de forma simples

--

### Funções padronizadas:

`st_*()`<br>
`gdal_*()`

<br><br>

Fonte: [@allison_horst](https://twitter.com/allison_horst)

---

# 4 Carregar dados vetoriais

## Importar dados 
```{r,,eval=FALSE}
# land use
polygons_land_use <- sf::st_read("SP_3543907_USO.shp") # pode demorar
polygons_land_use
plot(polygons_land_use$geometry)
```

```{r,,echo=FALSE,message=FALSE,results='hide',fig.align='center',out.width='50%'}
# land use
polygons_land_use <- sf::st_read("data/SP_3543907_USO.shp")
plot(polygons_land_use$geometry)
```

---

# 4 Carregar dados vetoriais

## Importar dados do pacote `geobr`
```{r,,eval=FALSE,warning=FALSE,message=FALSE}
# rio claro limit
rio_claro_limit <- geobr::read_municipality(code_muni = 3543907, year = 2015)
rio_claro_limit
plot(rio_claro_limit$geom)
```

```{r,,echo=FALSE,message=FALSE,results='hide',fig.align='center',out.width='50%'}
# rio claro limit
rio_claro_limit <- geobr::read_municipality(code_muni = 3543907, year = 2015)
plot(rio_claro_limit$geom)
```

---

class: inverse, middle, center

# 5 Mapas usando ggplot2

---

class: clear, center, middle
background-image: url(img/package_ggplot2.png)
background-size: 400px
background-position: 50% 50%

---

background-image: url(img/stats_illustrations_ggplot2_obra_prima_pt.png)
background-size: 450px
background-position: 95% 80%

# 5 Mapas usando ggplot2

## **ggplot2**

### Integrado ao **tidyverse**, possui uma sintaxe própria

--

### Necessita de funções específicas para objetos de **classes diferentes**

--

### Estruturado dessa forma:

`ggplot() +`<br>
`aes() +`<br>
`geom_() +`<br>
`facet_() +`<br>
`stats_() +`<br>
`coord_() +`<br>
`theme_()`<br>

Fonte: [@allison_horst](https://twitter.com/allison_horst)

---

background-image: url(img/cover_grammar_graphics.jpg),url(img/ggplot2_structure.png)
background-size: 160px,630px
background-position: 7% 55%,85% 55%

# 5 Mapas usando ggplot2

## The Grammar of Graphics (1999)

O pacote `ggplot2` implementa uma adaptação da **gramática dos gráficos** em "layers"

<br><br><br><br><br><br><br><br><br><br><br>

Fonte: https://medium.com/tdebeus/think-about-the-grammar-of-graphics-when-improving-your-graphs-18e3744d8d18

---

background-image: url(img/plot_ggplot.gif)
background-size: 750px
background-position: 50% 45%

# 5 Mapas usando ggplot2

## **ggplot2**

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: https://medium.com/tdebeus/think-about-the-grammar-of-graphics-when-improving-your-graphs-18e3744d8d18

---

# 5 Mapas usando ggplot2

### Mapa do limite de Rio Claro/SP
```{r,,fig.align='center',out.width='45%'}
# rio claro limit
ggplot() +
  geom_sf(data = rio_claro_limit)
```

---

# 5 Mapas usando ggplot2

### Mapa do limite de Rio Claro/SP - cor e preenchimento
```{r,,fig.align='center',out.width='45%'}
# rio claro limit color and fill
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA)
```

---

# 5 Mapas usando ggplot2

### Limite e uso e cobertura da terra
```{r,,fig.align='center',out.width='45%'}
# rio claro limit fill + land use
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  geom_sf(data = polygons_land_use)
```

---

# 5 Mapas usando ggplot2

### Limite e uso e cobertura da terra com cores
```{r,,fig.align='center',out.width='45%'}
# rio claro limit fill + land use with colors
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA)
```

---

# 5 Mapas usando ggplot2

### Limite e uso e cobertura da terra com cores (inverter)
```{r,,fig.align='center',out.width='45%'}
# land use with colors + rio claro limit fill
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) 
```

---

# 5 Mapas usando ggplot2

### Definir cores para as classes
```{r,,fig.align='center',out.width='45%'}
# land use and choose colors + rio claro limit fill 
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green"))
```

---

# 5 Mapas usando ggplot2

### Mudar o CRS (Sistema de Coordenadas)
```{r,,fig.align='center',out.width='40%'}
# land use and choose colors + rio claro limit fill + coords
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green")) +
  coord_sf(datum = sf::st_crs(polygons_land_use))
```

---

# 5 Mapas usando ggplot2

### Mudar o tema
```{r,,fig.align='center',out.width='35%'}
# land use and choose colors + rio claro limit fill + coords + themes
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green")) +
  coord_sf(datum = sf::st_crs(polygons_land_use)) +
  theme_bw()
```

---

# 5 Mapas usando ggplot2

### Adicionar barra de escala e norte
```{r,,eval=FALSE}
# land use and choose colors + rio claro limit fill + coords + themes + scalebar + north
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green")) +
  coord_sf(datum = sf::st_crs(polygons_land_use)) +
  theme_bw() +
  annotation_scale(location = "br", width_hint = .3) +
  annotation_north_arrow(location = "br", which_north = "true", 
                         pad_x = unit(0, "cm"), pad_y = unit(.8, "cm"),
                         style = north_arrow_fancy_orienteering)
```

---

# 5 Mapas usando ggplot2

### Adicionar barra de escala e norte
```{r,,echo=FALSE,fig.align='center',out.width='65%'}
# land use and choose colors + rio claro limit fill + coords + themes + scalebar + north
ggplot() +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA) +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  scale_fill_manual(values = c("blue", "orange", "gray30", "forestgreen", "green")) +
  coord_sf(datum = sf::st_crs(polygons_land_use)) +
  theme_bw() +
  annotation_scale(location = "br", width_hint = .3) +
  annotation_north_arrow(location = "br", which_north = "true", 
                         pad_x = unit(0, "cm"), pad_y = unit(.8, "cm"),
                         style = north_arrow_fancy_orienteering)
```

---

# 5 Mapas usando ggplot2

### Exportar
```{r,,eval=FALSE}
# export
ggsave(filename = "map_rio_claro_land_use.png",
       path = "/home/mude/data/gitlab/liver/werlive_02/data",
       width = 20, 
       height = 20, 
       units = "cm", 
       dpi = 300)
```

---

class: inverse, middle, center

# 6. Considerações finais

---

background-image: url(img/cheatsheet_sf.png)
background-size: 600px
background-position: 50% 50%

# Dica de material

## Simple Features (sf) Cheat Sheet

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://github.com/rstudio/cheatsheets/raw/master/sf.pdf

---

background-image: url(img/cheatsheet_ggplot2.png)
background-size: 600px
background-position: 50% 50%

# Dica de material

## Data Visualization Cheatsheet

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf

---

background-image: url(img/cover_ggplot2.jpg)
background-size: 230px
background-position: 50% 63%

# Dica de livros

## Livros

### ggplot2 (2009, 2016)

<br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://ggplot2.tidyverse.org/

---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)