---
title: "We R Live 09: Introdução à Estatística Espacial III<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Felipe Sodré M. Barros<br> Maurício Vancine <br> "
date: "<br> 30/06/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 09

## Tópicos
### <u><b>Introdução (40 min.)</b></u>
#### 1 Desafio da We R Live 09
#### 2 Pacotes a serem usados
#### 3 Considerações conceituais  

  * 3.1 Revisão análise de primeira ordem
  * 3.2 Revisão análise de segunda ordem
  * 3.3 Análise de segunda ordem multivariada (bivariada)

#### Mão na massa (30/40 min.)

#### 4 Considerações finais (5 min)

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

<br>

### 1. Apoie as iniciativas do GeoCast Brasil:
- Não deixe de curtir as *lives* e videos;
- Não deixe de se inscrever no canal;
- Ajude divulgando nas redes sociais;

### 2. Lives passadas: visite nosso site:
- Site: https://werlive.netlify.app/

### 3. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

<br>
.center[
[`r icon::fa_twitter(size = 2)`](https://twitter.com/GeoCastBrasil)
[`r icon::fa_gitlab(size = 2)`](https://gitlab.com/geocastbrasil/liver)
[`r icon::fa_instagram(size = 2)`](https://www.instagram.com/geocastbrasil/)
[`r icon::fa_facebook(size = 2)`](https://facebook.com/GeoCastBrasil/)]

---

class: inverse, middle, center

# 1 Desafio da We R Live 09

---

# 1 Desafio da We R Live 09

### Introdução Estatítica Espacial
- Introdução à análise de processos pontuais III
  - Análise de primeira ordem `r icon::fa_check()`
  - Análise de segunda ordem `r icon::fa_check()`
  - Análise de segunda ordem bivariada 

### Extras
Download de dados:
- fogo cruzado (`crossfire`)
- OpenStreetMap (`osmdata`)

--

background-image: url(img/livro_rstats.jpeg)
background-size: 250px
background-position: 80% 95%

---
background-image: url(img/Noticia_Violencia.png)
background-size: 500px
background-position: 50% 90%

# 1 Desafio da We R Live 09
## Estudo de caso
--

Será que os tiroteios ocorridos em 2019 tenderam a ocorrer próximos às escolas?

---
background-image: url(./img/package_r.png), url(./img/logo_fogo_cruzado_v2.png),url(./img/osmhex.png)
background-size: 150px, 250px, 100px
background-position: 95% 10%, 35% 50%, 35% 65%

# 2 Pacotes a serem usados

### Os mesmos usados nas lives anteriores

```{r eval=FALSE}
install.packages( c("spatstat", "rgdal", "sp", 
                    "maptools", "raster", "sf",
                    "readr", "dplyr", "geobr", "tmap"), 
                 dependencies = TRUE)
```
### Mais...
### [**`crossfire`**](https://fogocruzado.org.br/)
### [**`osmdata`**](https://github.com/ropensci/osmdata)
<br>
```{r eval=FALSE}
install.packages( c("crossfire", "osmdata"), 
                 dependencies = TRUE)
```

---

class: inverse, middle, center

# 3 Considerações conceituais

---

class: inverse, middle, center

# 3 Considerações conceituais
<br><br>
# 3.1 Revisão análise de primeira ordem

---

# 3 Considerações conceituais

## 3.1 Revisão análise de primeira ordem

### Processos pontuais

Processos pontuais -> Processo estocástico onde temos eventos observados em algumas localidades de uma região, mapeados como ponto: $(x,y)$.


Dentre os objetivos, busca-se identificar a exitência de um padrão de distribuiução espacial, a partir dos eventos observados. 


Esse padrão de distribuição espacial pode ser uma informação útil para entender influencia de outros fatores no desenvolvimento do processo estudado.

---
# 3  Considerações conceituais

## 3.1 Revisão análise de primeira ordem

### **Hipótese nula**

**Modelo conceitual: Complete Spatial Randomness - CSR**

**Homogeneous Poisson Point Process (HPP):**
- Intensidade/Densidade de eventos é constante na região de estudo;
    $\lambda(x) = \lambda > 0$
- Todos os eventos são independentes;

**HPP = CSR**

--

`r icon::fa_exclamation_triangle()` Na maioria dos casos, o pressuposto de **HPP** não é válido.

**Inhomogeneous Poisson Point Process (IPP)**
- Independencia entre eventos, mas;
- Intensidade varia espacialmente;

---
# 3  Considerações conceituais

## 3.1 Revisão análise de primeira ordem

Também considerada como análises globais ou de larga escala: analisam a variação do valor médio de eventos no espaço - **intensidade** e a **densidade** de evento.

$\hat\lambda = \frac{n}{a}$

--

background-image: url(./img/kernel.png)
background-size: 700px
background-position: 60% 60%

--

background-image: url(./img/kernel_fig.png)
background-size: 600px
background-position: 60% 60%
<br><br><br><br><br><br><br><br><br>
[Fonte: geografia.ufes.br](http://www.geo.ufes.br/sites/geografia.ufes.br/files/field/anexo/m_bergamasch.pdf)

---
# 3 Considerações conceituais

## 3.1 Revisão análise de primeira ordem

Também considerada como análises globais ou de larga escala: analisam a variação do valor médio de eventos no espaço - **intensidade** e a **densidade** de evento.

$\hat\lambda = \frac{n}{a}$

`r icon::fa_lightbulb()` **Resumo: Analisa como a distribuição de determiando processo varia na área de estudo.**

--

`r icon::fa_exclamation_triangle()` Não se está analisando a **estrutura** de distribuição (aglomerado/inibição) do processo.

---

class: inverse, middle, center

# 3 Considerações conceituais
<br><br>
# 3.2 Revisão análise de segunda ordem

---

# 3  Considerações conceituais

## 3.2 Revisão análise de segunda ordem

São análises que **baseiam-se nas distancias**, para entender a **estrutura** de distribuição do processo estudado.

--

São, também chamadas de análises locais ou de pequena escala. [Bailey e Gatrel (1995), Câmara e Carvalho (2005) e Waller e Gotway (2004)]

--

Câmara e Carvalho (2005) e Waller e Gotway (2004) consideram as análises de
segunda ordem como estimadores de dependência espacial.

--

background-image: url(./img/1st_2nd_order_property.png)
background-size: 500px
background-position: 50% 100%
<br><br><br><br><br><br><br><br>
[Fonte](https://mgimond.github.io/Spatial/point-pattern-analysis.html#distance-based-analysis)
---

class: inverse, middle, center

# 3 Considerações conceituais
<br><br>
# 3.3 Análise multivariada de segunda ordem

---
# 3  Considerações conceituais

## 3.3 Análise multivariada de segunda ordem

São análises de segunda ordem usadas em processos pontuais que tenham alguma informação associada à localização (quantitativas ou qualitativas/categóricas) (Ex. identificação da espécie, altitude de ocorrência, etc.);  

--

Por isso são chamados multivariados e a essa informação extra se é dado o nome de "marcas" (também chamado de atributos nos SIGs).

--

Assim, será através da análise espacial de processos pontuais multivariados que poderemos avaliar a estrutura de distribuiçãoe spacial existente entre os atributos e os padrões de distribuição observados.

---

# 3  Considerações conceituais

## 3.3 Análise multivariada de segunda ordem

No caso, da análise bivariada, estamos trabalhando apenas com duas `marcas` (atributos) categóricos: $X_{i}$ e $X_{j}$ .

Com essa análise buscaremos inferir a tendência de um evento ocorrer de forma aleatória, regular ou agregado ao outro.

--

Esta função pode ser utilizada, ainda para analisar os eventos com diferentes atributos de um único processo pontual (**multivariada**). 

Exemplo: Análise da distribuição de uma espécie levando-se em consideração a altitude onde a coleta foi realizada.

--

Nesta análise, calcula-se as distâncias entre eventos com um determinado valor de atributo ( $X_{i}$ ) para eventos com valores diferentes ( $X_{j}$ )... até ( $X_{n}$ ). 

---

# 3 Considerações conceituais

## 3.3 Análise multivariada de segunda ordem

### Valores estimados

A leitura dos valores resultantes das funções, se mantêm iguais:

`L() < 0`: indica que há poucos eventos $X_{i}$ vizinhos aos $X_{j}$ dentro do raio r (tendência de inibição - distribuição regular);

--

`L () > 0`: evidencia-se a tendência de interação entre os eventos $X_{i}$ e $X_{j}$ dada a elevada quantidade de evento $X_{j}$ proximos aos eventos $X_{i}$ (distribuição aglomerada para a distância r).

---
background-image: url(./img/Exemplos_K.png)
background-size: 800px
background-position: 50% 50%

# 3 Considerações conceituais

---

# 3 Considerações conceituais

## 3.3 Análise multivariada de segunda ordem

### Níveis de significância

Uma forma de identificar o nível de significância das análises de segunda ordem, é simulando `m` distribuições espaciais baseadas na ( $h_{0}$ ).

A simulação respeita a intensidade $\lambda()$ de cada processo observado.

---

class: inverse, middle, center

# Referencias!!! `r icon::fa_book_open()`

---
# Referencias

`r icon::fa_book_reader()`   **Todo o conteúdo exposto e muito mais está baseado no Trabalho de Conclusão de Curso do Felipe**. O mesmo se encontra disponível [aqui](https://gitlab.com/geocastbrasil/liver/-/blob/master/static/docs/Barros_FELIPE_ANALISE_PROCESSOS_PONTUAIS.pdf).

BAILEY, Trevor C. e GATRELL, Antony C.. Introductory Methods for Point Patterns. In: Interactive Spatial Data Analysis. 1° Edição. Harlow, Inglaterra. Prentice Hall; 1995

CÂMARA, G.; CARVALHO, M.S.; Análise Espacial de Eventos Pontuais. In: Análise Espacial de Dados Geográficos. 1° Edição. São Paulo, Brasil. INPE; 2005

WALLER, Lance A. e GOTWAY Carol A.. Analysis of spatial point process. In: Applied Spatial Statistics for Public Health Data.Edição. Georgia, EUA. Wiley; 2004

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive09/werlive09.R`]

---

class: inverse, middle, center

# 4 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

# 4 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`
## 4.1 Possíveis aplicações

--

background-image: url(img/Paper_datamining.png)
background-size: 425px
background-position: 50% 50%

[Link artigo](https://www.sciencedirect.com/science/article/abs/pii/S0957417415005783)
---
background-image: url(img/Blog_JohnSnow.png)
background-size: 425px
background-position: 50% 50%

# 4 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`
## 4.1 Possíveis aplicações

[Link artigo](https://geoind.wordpress.com/2013/12/23/john_snow_revisitado/)
---
background-image: url(img/FastFood.png)
background-size: 425px
background-position: 50% 50%

# 4 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`
## 4.1 Possíveis aplicações

[Link artigo](https://ajph.aphapublications.org/doi/full/10.2105/AJPH.2004.056341)


---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)
